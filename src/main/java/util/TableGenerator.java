package util;

import javax.swing.table.DefaultTableModel;
import java.lang.reflect.Field;
import java.util.List;

public class TableGenerator {
	
	public static void generateTable(List<? extends Object> objects, DefaultTableModel tableModel) throws IllegalArgumentException, IllegalAccessException{
		Field[] fields = objects.get(0).getClass().getDeclaredFields();
		Object[] header = new Object[fields.length];
		Object[] values = new Object[fields.length];
		int i;
		for(i = 0; i < fields.length; i++){
			header[i] = fields[i].getName();
		}
		tableModel.addRow(header);
		for (Object object : objects){
			for(i = 0; i < fields.length; i++){
				fields = object.getClass().getDeclaredFields();
				fields[i].setAccessible(true);
				values[i] = fields[i].get(object);
			}
			tableModel.addRow(values);
		}
	}

	public static void generateTable(Object object, DefaultTableModel tableModel) throws IllegalArgumentException, IllegalAccessException{
		Field[] fields = object.getClass().getDeclaredFields();
		Object[] header = new Object[fields.length];
		Object[] values = new Object[fields.length];
		int i;
		for(i = 0; i < fields.length; i++){
			header[i] = fields[i].getName();
		}
		tableModel.addRow(header);
		for(i = 0; i < fields.length; i++){
			fields = object.getClass().getDeclaredFields();
			fields[i].setAccessible(true);
			values[i] = fields[i].get(object);
		}
		tableModel.addRow(values);
	}

}
