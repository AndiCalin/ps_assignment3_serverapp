package util;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionConfiguration {

    Connection connection = null;

    public static Connection getConnection()  throws ClassNotFoundException  {

        Connection connection = null;
        String url = "jdbc:mysql://localhost:3306/psa3";
        String user = "root";
        String password = "Calin_Andrei";
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection(url, user, password);
            System.out.println("Connection OK!");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }
}
