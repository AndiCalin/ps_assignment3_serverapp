package DAO;

import entity.Article;
import util.ConnectionConfiguration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ArticleDAO {
	
	public List<Article> getArticleInfo(){
		try {
			Connection connection = ConnectionConfiguration.getConnection();
			PreparedStatement prep = connection.prepareStatement("SELECT * FROM articles");
			ResultSet resultSet = prep.executeQuery();
			Article article = null;
			List<Article> articles = new ArrayList<Article>();
			while (resultSet.next()){
				article = new Article();
				article.setId(resultSet.getInt("id"));
				article.setAuthorId(resultSet.getInt("authorId"));
				article.setTitle(resultSet.getString("title"));
				article.setAbstractOfArticle(resultSet.getString("abstract"));
				article.setBody(resultSet.getString("body"));
				articles.add(article);
			}
			connection.close();
			return articles;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Article getArticleById(int id){
			try {
				Connection connection = ConnectionConfiguration.getConnection();
				PreparedStatement prep = connection.prepareStatement("SELECT * FROM articles WHERE id = ?");
				prep.setInt(1, id);
				ResultSet resultSet = prep.executeQuery();
				Article article = null;
				while (resultSet.next()){
					article = new Article();
					article.setId(resultSet.getInt("id"));
                    article.setAuthorId(resultSet.getInt("authorId"));
					article.setTitle(resultSet.getString("title"));
					article.setAbstractOfArticle(resultSet.getString("abstract"));
					article.setBody(resultSet.getString("body"));
				}
				connection.close();
				return article;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
	}

    public Article getArticleByTitle(String title){
        try {
            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM articles WHERE title = ?");
            prep.setString(1, title);
            ResultSet resultSet = prep.executeQuery();
            Article article = null;
            while (resultSet.next()){
                article = new Article();
                article.setId(resultSet.getInt("id"));
                article.setAuthorId(resultSet.getInt("authorId"));
                article.setTitle(resultSet.getString("title"));
                article.setAbstractOfArticle(resultSet.getString("abstract"));
                article.setBody(resultSet.getString("body"));
            }
			connection.close();
            return article;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
	
	public boolean insertArticle(Article article){
		try {
			Connection connection = ConnectionConfiguration.getConnection();
			PreparedStatement prep = connection.prepareStatement("INSERT INTO articles (authorId, title, abstract, body) VALUES (?, ?, ?, ?)");
            prep.setInt(1, article.getAuthorId());
			prep.setString(2, article.getTitle());
			prep.setString(3, article.getAbstractOfArticle());
			prep.setString(4, article.getBody());
			prep.executeUpdate();
			connection.close();
			return true;
		} catch (Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public boolean updateArticle(Article article){
        try {
            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement prep = connection.prepareStatement("UPDATE articles SET authorId = ?, title = ?, abstract = ?, body = ? WHERE id = ?");
            prep.setInt(1, article.getAuthorId());
            prep.setString(2, article.getTitle());
            prep.setString(3, article.getAbstractOfArticle());
            prep.setString(4, article.getBody());
            prep.setInt(5, article.getId());
            prep.executeUpdate();
			connection.close();
            return true;
        } catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public static boolean deleteArticle(int articleId){
	    try {
            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement prep = connection.prepareStatement("DELETE FROM articles WHERE id = ?");
            prep.setInt(1, articleId);
            prep.executeUpdate();
			connection.close();
            return true;
        } catch (Exception e){
	        e.printStackTrace();
        }
        return false;
    }

}
