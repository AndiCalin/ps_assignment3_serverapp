package DAO;

import entity.User;
import util.ConnectionConfiguration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {

    public User getUserById(int id){
        try{
            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM users WHERE id = ?");
            prep.setInt(1, id);
            ResultSet resultSet = prep.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setUserName(resultSet.getString("userName"));
                user.setPassword(resultSet.getString("password"));
                user.setIsAdmin(resultSet.getBoolean("isAdmin"));
                return user;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public User getUserByName(String name){
        try{
            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM users WHERE userName = ?");
            prep.setString(1, name);
            ResultSet resultSet = prep.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setUserName(resultSet.getString("userName"));
                user.setPassword(resultSet.getString("password"));
                user.setIsAdmin(resultSet.getBoolean("isAdmin"));
                return user;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public List<User> getAllUsers(){
        try{
            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM users");
            ResultSet resultSet = prep.executeQuery();
            User user = null;
            List<User> users = new ArrayList<User>();
            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setUserName(resultSet.getString("userName"));
                user.setPassword(resultSet.getString("password"));
                user.setIsAdmin(resultSet.getBoolean("isAdmin"));
                users.add(user);
            }
            return users;
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public boolean insertUser(User user){
        try {
            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement prep = connection.prepareStatement("INSERT INTO users (userName, password, isAdmin) VALUES (?, ?, ?)");
            prep.setString(1, user.getUserName());
            prep.setString(2, user.getPassword());
            prep.setBoolean(3, false);
            prep.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateUser(User user){
        try {
            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement prep = connection.prepareStatement("UPDATE users SET userName = ?, password = ? WHERE id = ?");
            prep.setString(1, user.getUserName());
            prep.setString(2, user.getPassword());
            prep.setInt(3, user.getId());
            prep.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteUser(int userId){
        try {
            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement prep = connection.prepareStatement("DELETE FROM users WHERE id = ?");
            prep.setInt(1, userId);
            prep.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
