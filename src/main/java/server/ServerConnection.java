package server;


import com.fasterxml.jackson.databind.ObjectMapper;
import entity.TransportObject;
import org.json.JSONObject;
import service.ArticleService;
import service.UserService;
import util.ConnectionConfiguration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Connection;

public class ServerConnection extends Thread {

    private static BufferedReader in;
    private static PrintWriter out;
    private Socket socket;
    private ObjectMapper objectMapper;
    private ArticleService articleService;
    private UserService userService;
    private FunctionCaller functionCaller;

    public ServerConnection(Socket socket, BufferedReader in, PrintWriter out, ArticleService articleService, UserService userService) {
        this.socket = socket;
        this.in = in;
        this.out = out;
        this.userService = userService;
        this.articleService = articleService;
        this.objectMapper = new ObjectMapper();
        this.functionCaller = new FunctionCaller();
    }

    @Override
    public void run() {
        while (true) {
            try {

                String command = in.readLine();

                if (command != null) {
                    if (objectMapper.readValue(command, TransportObject.class).getCommandLine().equals("Exit")) {
                        System.out.println("Client Port Number " + this.socket.getPort() + " has quit the application!");
                        System.out.println("Closing connection!");
                        out.println("OK");
                        this.socket.close();
                        System.out.println("Connection has been closed!");
                        break;
                    }
                    String response = objectMapper.writeValueAsString(functionCaller.execute(command));
                    out.println(response);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}



 /*       try {
            //out = new PrintWriter(socket.getOutputStream(), true);
            //in = new BufferedReader(new InputStreamReader(
            //        socket.getInputStream()));

            while (true) {
                String command = in.readLine();
                if (in != null) {
                    TransportObject response = FunctionCaller.execute(command);
                    out.println(objectMapper.writeValueAsString(response));
                }
                Thread.sleep(1);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
                in.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

*/