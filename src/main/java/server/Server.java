package server;

import service.ArticleService;
import service.ArticleServiceImpl;
import service.UserService;
import service.UserServiceImpl;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    //private static BufferedReader in;
    //private static PrintWriter out;



    public static void main(String[] args) throws IOException, InterruptedException {
        ServerSocket listener = new ServerSocket(9090);
        ArticleService articleService = new ArticleServiceImpl();
        UserService userService = new UserServiceImpl();

        System.out.println("Server ready for connections!");
        while (true) {
            Socket  socket = null;
            try {
                socket = listener.accept();

                System.out.println("A new client is connected, port number " + socket.getPort());

                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        socket.getInputStream()));

                System.out.println("Assigning new thread for this client");

                // create a new thread object
                Thread thread = new ServerConnection(socket, in, out,articleService,userService);

                // Invoking the start() method
                thread.start();
            }
            catch (Exception e){
                socket.close();
                e.printStackTrace();
            }
        }
    }
}