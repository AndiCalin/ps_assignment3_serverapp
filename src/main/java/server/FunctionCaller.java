package server;

import com.fasterxml.jackson.databind.ObjectMapper;
import entity.Article;
import entity.TransportObject;
import entity.User;
import org.json.JSONObject;
import service.ArticleService;
import service.ArticleServiceImpl;
import service.UserService;
import service.UserServiceImpl;

import java.io.IOException;
import java.util.List;

public class FunctionCaller {

    public FunctionCaller(){

    }

    public TransportObject execute(String command) throws IOException{
        ArticleService articleService = new ArticleServiceImpl();
        UserService userService = new UserServiceImpl();
        TransportObject response = new TransportObject();
        response.setTransportObjectType(TransportObject.state.RESPONSE);
        response.setCommandParameter(""+0);
        response.setCommandLine("justAResponse");
        ObjectMapper objectMapper = new ObjectMapper();
        TransportObject transportObjectToBeExecuted = objectMapper.readValue(command, TransportObject.class);

        if (transportObjectToBeExecuted.getClassName().equals("Article")){
            Article article = null;
            response.setClassName("Article");
            if (transportObjectToBeExecuted.getCommandBody() != null) {
                article = objectMapper.readValue(transportObjectToBeExecuted.getCommandBody(), Article.class);
            }
            if (transportObjectToBeExecuted.getCommandLine().equals("InsertArticle")){
                if (articleService.insertArticle(article)) {
                    response.setCommandBody("true");
                } else {
                    response.setCommandBody("false");
                }return response;
            } else if (transportObjectToBeExecuted.getCommandLine().equals("UpdateArticle")){
                if (articleService.updateArticle(article)) {
                    response.setCommandBody("true");
                } else {
                    response.setCommandBody("false");
                }
                return response;
            } else if (transportObjectToBeExecuted.getCommandLine().equals("DeleteArticle")){
                if (articleService.deleteArticle(Integer.parseInt(transportObjectToBeExecuted.getCommandParameter()))) {
                    response.setCommandBody("true");
                } else {
                    response.setCommandBody("false");
                }
                return response;
            } else if (transportObjectToBeExecuted.getCommandLine().equals("ListAll")){
                List<Article> articles = articleService.getArticleInfo();
                response.setCommandBody(objectMapper.writeValueAsString(articles));
                return response;
            } else if (transportObjectToBeExecuted.getCommandLine().equals("SearchById")){
                article = articleService.getArticleById(Integer.parseInt(transportObjectToBeExecuted.getCommandParameter()));
                response.setCommandBody(objectMapper.writeValueAsString(article));
                return response;
            } else if (transportObjectToBeExecuted.getCommandLine().equals("SearchByTitle")){
                article = articleService.getArticleByTitle(transportObjectToBeExecuted.getCommandParameter());
                response.setCommandBody(objectMapper.writeValueAsString(article));
                return response;
            } else if (transportObjectToBeExecuted.getCommandLine().equals("ListAllForUser")){
                List<Article> articles = articleService.getArticleInfoForUser(Integer.parseInt(transportObjectToBeExecuted.getCommandParameter()));
                response.setCommandBody(objectMapper.writeValueAsString(articles));
                return response;
            }
        } else if (transportObjectToBeExecuted.getClassName().equals("User")){
            User user = null;
            response.setClassName("User");
            if (transportObjectToBeExecuted.getCommandBody() != null){
                user = objectMapper.readValue(transportObjectToBeExecuted.getCommandBody(), User.class);
            }
            if (transportObjectToBeExecuted.getCommandLine().equals("InsertUser")){
                if (userService.insertUser(user)){
                    response.setCommandBody("true");
                } else {
                    response.setCommandBody("false");
                }
                return response;
            } else if (transportObjectToBeExecuted.getCommandLine().equals("UpdateUser")){
                if (userService.updateUser(user)){
                    response.setCommandBody("true");
                } else {
                    response.setCommandBody("false");
                }
                return response;
            } else if (transportObjectToBeExecuted.getCommandLine().equals("DeleteUser")){
                if (userService.deleteUser(Integer.parseInt(transportObjectToBeExecuted.getCommandParameter()))){
                    response.setCommandBody("true");
                } else {
                    response.setCommandBody("false");
                }
                return response;
            } else if (transportObjectToBeExecuted.getCommandLine().equals("SearchById")){
                user = userService.getUserById(Integer.parseInt(transportObjectToBeExecuted.getCommandParameter()));
                response.setCommandBody(objectMapper.writeValueAsString(user));
                return response;
            } else if (transportObjectToBeExecuted.getCommandLine().equals("SearchByName")){
                user = userService.getUserByName(transportObjectToBeExecuted.getCommandParameter());
                response.setCommandBody(objectMapper.writeValueAsString(user));
                return response;
            } else if (transportObjectToBeExecuted.getCommandLine().equals("ListAll")){
                List<User> users = userService.getAllUsers();
                response.setCommandBody(objectMapper.writeValueAsString(users));
                return response;
            }
        }
        return null;
    }

}
