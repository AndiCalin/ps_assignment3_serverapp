package service;

import DAO.ArticleDAO;
import entity.Article;

import java.util.ArrayList;
import java.util.List;

public class ArticleServiceImpl implements ArticleService {

	ArticleDAO articleDAO = new ArticleDAO();
	
	public List<Article> getArticleInfo() {
		return articleDAO.getArticleInfo();
	}

	public List<Article> getArticleInfoForUser(int id) {
		List<Article> articles = articleDAO.getArticleInfo();
		List<Article> articlesForUser = new ArrayList<Article>();
		for (Article article : articles){
			if (article.getAuthorId() == id){
				articlesForUser.add(article);
			}
		}
		return articlesForUser;
	}

	public Article getArticleById(int id) {
		return articleDAO.getArticleById(id);
	}

	public Article getArticleByTitle(String title) {
		return articleDAO.getArticleByTitle(title);
	}

	public boolean insertArticle(Article article) {
		return articleDAO.insertArticle(article);
	}

	public boolean updateArticle(Article article){
		Article article1 = getArticleById(article.getId());
		if (article1 == null){
			throw new RuntimeException("Article not found!");
		} else {
			articleDAO.updateArticle(article);
			return true;
		}
	}

	public boolean deleteArticle(int articleId){
		Article article = getArticleById(articleId);
		if (article == null){
			throw new RuntimeException("Article not found!");
		} else {
			articleDAO.deleteArticle(articleId);
			return true;
		}
	}
}
