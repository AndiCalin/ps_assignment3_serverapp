package service;

import entity.User;

import java.util.List;

public interface UserService {

    User getUserById(int userId);
    User getUserByName(String userName);
    List<User> getAllUsers();
    boolean insertUser(User user);
    boolean updateUser(User user);
    boolean deleteUser(int userId);

}
