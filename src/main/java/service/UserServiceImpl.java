package service;

import DAO.UserDAO;
import entity.User;

import java.util.List;

public class UserServiceImpl implements UserService {

    UserDAO userDAO = new UserDAO();

    public User getUserById(int userId){
        User user = userDAO.getUserById(userId);
        if (user == null){
            throw new RuntimeException("No user found!");
        } else {
            return user;
        }
    }

    public User getUserByName(String userName){
        User user = userDAO.getUserByName(userName);
        if (user == null){
            throw new RuntimeException("No user found!");
        } else {
            return user;
        }
    }

    public List<User> getAllUsers(){
        List<User> users = userDAO.getAllUsers();
        if (users.size() == 0){
            throw new RuntimeException("No users found!");
        } else {
            return users;
        }
    }

    public boolean insertUser(User user){
        return userDAO.insertUser(user);
    }

    public boolean updateUser(User user){
        User user1 = userDAO.getUserById(user.getId());
        if (user1 == null){
            throw new RuntimeException("User not found!");
        } else {
            return userDAO.updateUser(user);
        }
    }

    public boolean deleteUser(int userId){
        User user1 = userDAO.getUserById(userId);
        if (user1 == null){
            throw new RuntimeException("User not found!");
        } else {
            return userDAO.deleteUser(userId);
        }
    }

}
