package service;

import entity.Article;

import java.util.List;

public interface ArticleService {
	
	List<Article> getArticleInfo();

	List<Article> getArticleInfoForUser(int id);
	
	Article getArticleById(int id);

    Article getArticleByTitle(String title);

	boolean insertArticle(Article article);

	boolean updateArticle(Article article);

	boolean deleteArticle(int articleId);

}
